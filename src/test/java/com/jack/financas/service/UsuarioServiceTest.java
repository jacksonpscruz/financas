package com.jack.financas.service;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jack.financas.model.Usuario;
import com.jack.financas.repository.UsuarioRepository;

class UsuarioServiceTest {

	@Autowired
	UsuarioRepository repository;
	
	@Autowired
	UsuarioService service;
	
	Usuario usuario;
	
//	@BeforeEach
	void before() {

		this.usuario = Usuario.builder()
			.nome("jackson cruz")
			.email("jackson@tcs.com").build();
		
		repository.save(usuario);
	}
	
//	@Test// nao deve lançar exceção
	void test_deve_validar_email() {
		
		repository.deleteAll();
		
		service.validarEmail("jackson@tcs.com");
	}

//	@Test// deve lançar exceção negocioException
	void test_deve_lancar_erro_ao_validar_email_quando_existir_email_cadastrado() {
		
		service.validarEmail("jackson@tcs.com");
	}
	
//	@AfterEach
	void after() {
		repository.delete(this.usuario);
	}

}
