package com.jack.financas.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import com.jack.financas.model.Usuario;

//@ActiveProfiles("test")
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE)
class UsuarioRepositoryTest {

//	@Autowired
	UsuarioRepository repository;
	
//	@Autowired
	TestEntityManager em;
	
//	@Test
	void test_verifica_se_existe_um_email() {

		Usuario usuario = Usuario.builder()
				.nome("jackson cruz")
				.email("jackson.cruz@tcs.com").build();
		
		em.persist(usuario);
		
		boolean result = repository.existsByEmail("jackson.cruz@tcs.com");
		assertThat(result).isTrue();
	}

//	@Test
	void test_deve_retornar_falso_quando_nao_houver_usuario_cadastrado_com_email() {
		
		boolean result = repository.existsByEmail("jackson.cruz@tcs.com");
		
		assertThat(result).isFalse();
	}
	
//	@Test
	void test_deve_salvar_usuario() {
		
		Usuario usuario = Usuario.builder().nome("usuario")
				.email("usuario@email.com")
				.senha("123").build();
		
		Usuario save = repository.save(usuario);
		
		assertThat(save.getId()).isNotNull();
		
	}

}
