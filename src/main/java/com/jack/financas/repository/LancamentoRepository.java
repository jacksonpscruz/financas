package com.jack.financas.repository;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jack.financas.model.Lancamento;
import com.jack.financas.model.enums.TipoLancamento;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long> {

	@Query(value = "SELECT SUM(l.valor) FROM Lancamento l JOIN l.usuario u"
			+ " WHERE u.id =:id AND l.tipo =:tipo"
			+ " GROUP BY u")
	public BigDecimal obterSaldoPorTipoLancamentoEUsuario(@Param(value = "id") Long id, @Param(value = "tipo") TipoLancamento tipo);
	
}
