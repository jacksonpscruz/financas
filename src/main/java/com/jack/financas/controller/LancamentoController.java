package com.jack.financas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jack.financas.controller.dto.AtualizaStatusDTO;
import com.jack.financas.controller.dto.LancamentoDTO;
import com.jack.financas.exception.NegocioException;
import com.jack.financas.model.Lancamento;
import com.jack.financas.service.LancamentoService;
import com.jack.financas.service.UsuarioService;

@SuppressWarnings({"rawtypes", "unchecked"})

@RestController
@RequestMapping("/api/lancamentos")
public class LancamentoController {

	@Autowired
	private LancamentoService service;

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping
	public ResponseEntity buscar(@RequestParam Long usuario,
							     @RequestParam(required = false) String descricao, 
								 @RequestParam(required = false) Integer mes,
								 @RequestParam(required = false) Integer ano) {

		final Lancamento filtro = Lancamento.builder()
				.descricao(descricao)
				.mes(mes)
				.ano(ano).build();
		
		try {
			final List<Lancamento> lancamentos = this.service.buscar(usuario, filtro);
			return ResponseEntity.ok(lancamentos);
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}

	@PostMapping
	public ResponseEntity salvar(@RequestBody LancamentoDTO dto) {

		try {
			final Lancamento lancamento = this.service.salvar(dto.lancamento(usuarioService));
			return new ResponseEntity(lancamento, HttpStatus.CREATED);
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}

	@PutMapping("/{id}")
	public ResponseEntity atualizar(@PathVariable Long id, @RequestBody LancamentoDTO dto) {

		try {
			final Lancamento lancamento = this.service.atualizar(id, dto.lancamento(usuarioService));
			return ResponseEntity.ok(lancamento);
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}

	@PatchMapping("/{id}/atualiza-status")
	public ResponseEntity atualizar(@PathVariable Long id, @RequestBody AtualizaStatusDTO dto) {
		
		try {
			final Lancamento lancamento = this.service.atualizarStatus(id, dto.getStatus());
			return ResponseEntity.ok(lancamento);
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}

	@PatchMapping("/{id}/cancelar")
	public ResponseEntity cancelar(@PathVariable Long id) {
		
		try {
			final Lancamento lancamento = this.service.cancelar(id);
			return ResponseEntity.ok(lancamento);
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity apagar(@PathVariable Long id) {
		try {
			this.service.apagar(id);
			return ResponseEntity.noContent().build();
		} catch (NegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
}
