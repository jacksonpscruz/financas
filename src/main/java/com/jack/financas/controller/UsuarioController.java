package com.jack.financas.controller;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jack.financas.controller.dto.UsuarioDTO;
import com.jack.financas.model.Usuario;
import com.jack.financas.service.LancamentoService;
import com.jack.financas.service.UsuarioService;

@SuppressWarnings({"rawtypes", "unchecked"})

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;
	
	@Autowired
	private LancamentoService serviceLancamento;

	@PostMapping
	public ResponseEntity salvar(@RequestBody UsuarioDTO dto) {
		
		try {
			final Usuario usuario= service.salvar(dto.usuario());
			return new ResponseEntity(usuario, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@PostMapping("/autenticar")
	public ResponseEntity autenticar(@RequestBody UsuarioDTO dto) {
		
		try {
			final Usuario autenticado = service.autenticar(dto.getEmail(), dto.getSenha());
			return ResponseEntity.ok(autenticado);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
	}
	
	@GetMapping("/{id}/saldo")
	public ResponseEntity obterSaldo(@PathVariable Long id) {
		
		Optional usuario = service.obterPorId(id);
		
		if (!usuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		BigDecimal saldo = serviceLancamento.obterSaldoPorUsuario(id);
		
		return ResponseEntity.ok(saldo);
	}
}
