package com.jack.financas.controller.dto;

import com.jack.financas.model.Usuario;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UsuarioDTO {

	private String nome;
	private String email;
	private String senha;
	
	public Usuario usuario() {
		return Usuario.builder()
				.nome(this.nome)
				.email(this.email)
				.senha(this.senha)
				.build();
	}
	
}
