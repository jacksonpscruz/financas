package com.jack.financas.controller.dto;

import java.math.BigDecimal;

import org.apache.commons.lang3.ObjectUtils;

import com.jack.financas.model.Lancamento;
import com.jack.financas.model.enums.StatusLancamento;
import com.jack.financas.model.enums.TipoLancamento;
import com.jack.financas.service.UsuarioService;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class LancamentoDTO {

	private Long id;
	private String descricao;
	private Integer mes;
	private Integer ano;
	private BigDecimal valor;
	private Long usuario;
	private String tipo;
	private String status;
	
	public Lancamento lancamento(UsuarioService usuarioService) {

		return Lancamento.builder()
				.id(this.id)
				.descricao(this.descricao)
				.mes(this.mes)
				.ano(this.ano)
				.valor(this.valor)
				.usuario(usuarioService.obterPorId(this.usuario).get())
				.tipo(TipoLancamento.valueOf(this.tipo))
				.status(StatusLancamento.valueOf(this.status))
				.build();
	}
	
}
