package com.jack.financas.model.enums;

public enum StatusLancamento {

	PENDENTE, CANCELADO, EFETIVADO;
}
