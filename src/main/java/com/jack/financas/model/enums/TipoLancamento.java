package com.jack.financas.model.enums;

public enum TipoLancamento {

	RECEITA, DESPESA;
}
