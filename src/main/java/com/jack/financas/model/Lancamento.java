package com.jack.financas.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jack.financas.model.enums.StatusLancamento;
import com.jack.financas.model.enums.TipoLancamento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "lancamento", schema = "financas")
public class Lancamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer mes;
	private Integer ano;

	private String descricao;

	@JoinColumn(name = "id_usuario")
	@ManyToOne
	private Usuario usuario;
	
	@Column(name = "valor")
	private BigDecimal valor;

	@Column(name = "data_cadastro")
	private LocalDate dataCadastro;
	
	@Enumerated(EnumType.STRING)
	private TipoLancamento tipo;
	
	@Enumerated(EnumType.STRING)
	private StatusLancamento status;
	
	public Lancamento efetivar() {
		this.status = StatusLancamento.EFETIVADO;
		return this;
	}

	public Lancamento cancelar() {
		this.status = StatusLancamento.CANCELADO;
		return this;
	}
	
}
