package com.jack.financas.resource;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "associados", excerptProjection = AssociadoProjection.class)
public interface AssociadoResource extends PagingAndSortingRepository<Associado, Long> {

}
