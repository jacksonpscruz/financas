package com.jack.financas.resource;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "tarefas", excerptProjection = TarefaProjection.class)
public interface TarefaResource extends PagingAndSortingRepository<Tarefa, Long> {

}
