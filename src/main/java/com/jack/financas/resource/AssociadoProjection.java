package com.jack.financas.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "associadoProjection", types = Associado.class)
public interface AssociadoProjection {

	Long getId();
	
	@Value("#{target.nome + ' - ' + target.email}")
	String getDescricao();
	
	List<Tarefa> getTarefas();
	
}
