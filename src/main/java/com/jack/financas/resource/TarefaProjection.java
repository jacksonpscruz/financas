package com.jack.financas.resource;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "tarefaProjection", types = Tarefa.class)
public interface TarefaProjection {

	Long getId();

	String getNome();

}
