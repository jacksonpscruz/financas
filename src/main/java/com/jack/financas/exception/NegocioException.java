package com.jack.financas.exception;

public class NegocioException extends RuntimeException {

	private static final long serialVersionUID = 739841478052080366L;

	public NegocioException(String message) {
		super(message);
	}

}
