package com.jack.financas.exception;

public class AutenticacaoException extends RuntimeException {

	private static final long serialVersionUID = -2714093852953322092L;

	public AutenticacaoException(String message) {
		super(message);
	}

}
