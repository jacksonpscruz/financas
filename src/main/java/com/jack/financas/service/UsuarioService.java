package com.jack.financas.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jack.financas.exception.AutenticacaoException;
import com.jack.financas.exception.NegocioException;
import com.jack.financas.model.Usuario;
import com.jack.financas.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository repository;
	
	public Usuario autenticar(String email, String senha) {
		
		Optional<Usuario> usuario = repository.findByEmail(email);
		
		if (!usuario.isPresent()) {
			throw new AutenticacaoException("Usuário não encontrado para o email informado");
		}

		if (!usuario.get().getSenha().equals(senha)) {
			throw new AutenticacaoException("Senha inválida");
		}
		
		return usuario.get();
		
	}
	
	@Transactional
	public Usuario salvar(Usuario usuario) {
		this.validarEmail(usuario.getEmail());
		return repository.save(usuario);
	}
	
	public Optional<Usuario> obterPorId(Long id) {
		return this.repository.findById(id);
	}
	
	public void validarEmail(String email) {
		
		if (repository.existsByEmail(email)) {
			throw new NegocioException("Já existem um com este email.");
		}
	}

	@Async
	public void backgroundValidation() throws InterruptedException {
		Thread.sleep(10000);
	}
}
