package com.jack.financas.service;

import static com.jack.financas.model.enums.TipoLancamento.DESPESA;
import static com.jack.financas.model.enums.TipoLancamento.RECEITA;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.jack.financas.exception.NegocioException;
import com.jack.financas.model.Lancamento;
import com.jack.financas.model.Usuario;
import com.jack.financas.model.enums.StatusLancamento;
import com.jack.financas.repository.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository repository;
	
	@Autowired
	private UsuarioService usuarioService;

	public void validar(Lancamento lancamento) {

		if (lancamento.getDescricao() == null || StringUtils.isEmpty(lancamento.getDescricao())) {
			throw new NegocioException("Descrição inválida");
		}
		if (lancamento.getMes() == null || lancamento.getMes() < 1 || lancamento.getMes() > 12) {
			throw new NegocioException("Mês inválido");
		}
		if (lancamento.getAno() == null || lancamento.getAno().toString().length() > 4) {
			throw new NegocioException("Ano inválido");
		}
		if (lancamento.getValor() == null || lancamento.getValor().compareTo(BigDecimal.ZERO) < 1) {
			throw new NegocioException("Valor inválido");
		}

	}

	@Transactional
	public Lancamento salvar(Lancamento lancamento) {
		this.validar(lancamento);
		return repository.save(lancamento);
	}

	@Transactional
	public Lancamento atualizar(Lancamento lancamento) {
		this.validar(lancamento);
		return repository.save(lancamento);
	}

	@Transactional(readOnly = true)
	public List<Lancamento> buscar(Long usuarioId, Lancamento filtro) {
		
		final Usuario usuario = usuarioService.obterPorId(usuarioId)
					.orElseThrow(() -> new NegocioException("Não há lancamentos para o este usuário"));
		
		filtro.setUsuario(usuario);
		
		Example<Lancamento> example = Example.of(filtro, ExampleMatcher.matching()
				.withIgnoreCase()
				.withStringMatcher(StringMatcher.CONTAINING));
		
		return repository.findAll(example);
	}


	public Lancamento atualizar(Long id, Lancamento lancamento) {
		
		return repository.findById(id).map(entity -> {
			lancamento.setId(entity.getId());
			return this.atualizar(lancamento);
		}).orElseThrow(() -> new NegocioException("Lançamento não encontrado"));
		
	}

	public Lancamento atualizarStatus(Long id, String status) {
		
		return repository.findById(id).map(entity -> {
			
			StatusLancamento statusLancamento = StatusLancamento.valueOf(status);
			
			if (statusLancamento == null) {
				throw new NegocioException("Não foi possível atualizar o status do Lançamento. Status inválido");
			}
			
			entity.setStatus(statusLancamento);
			return this.atualizar(entity);
		}).orElseThrow(() -> new NegocioException("Lançamento não encontrado"));
		
	}

	public Lancamento cancelar(Long id) {
		
		return repository.findById(id).map(entity -> {
			return this.atualizar(entity.cancelar());
		}).orElseThrow(() -> new NegocioException("Lançamento não encontrado"));
	}

	@Transactional
	public void apagar(Long id) {

		repository.delete(repository.findById(id)
				.orElseThrow(() -> new NegocioException("Lançamento não encontrado")));
	}

	public BigDecimal obterSaldoPorUsuario(Long id) {
		
		BigDecimal receitas = ObjectUtils.defaultIfNull(repository.obterSaldoPorTipoLancamentoEUsuario(id, RECEITA), BigDecimal.ZERO);
		BigDecimal despesas = ObjectUtils.defaultIfNull(repository.obterSaldoPorTipoLancamentoEUsuario(id, DESPESA), BigDecimal.ZERO);
		
		return receitas.subtract(despesas);
		
	}
}
